//
//  FrontDoorApp.swift
//  FrontDoor
//
//  Created by Andrew Stevenson on 14.05.22.
//

import SwiftUI

@main
struct FrontDoorApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
