//
//  Model.swift
//  FrontDoor
//
//  Created by Andrew Stevenson on 21.05.22.
//

import Foundation;
import Security;

final class Model: NSObject, ObservableObject, URLSessionDelegate {

	@Published var user = "" {
		didSet {
			self.needs_save = true;
		}
	};

	@Published var pass = "" {
		didSet {
			self.needs_save = true;
		}
	};

	@Published var working = false;
	@Published var msg = "";
	@Published var success = false;
	@Published var save_auth = false;

	var needs_save = false;
	private var host = "";

	init(fromKeychain host: String) {
		self.host = host;

		let query = [
		  kSecClass: kSecClassInternetPassword,
		  kSecAttrServer: host,
		  kSecReturnAttributes: true,
		  kSecReturnData: true
		] as CFDictionary

		var result: AnyObject?
		let status = SecItemCopyMatching(query, &result);

		if (status != errSecSuccess) {
			let msg = SecCopyErrorMessageString(status, nil)! as String;
			print("SecItemCopyMatching failed: \(msg) (\(status))");
			return;
		}

		if (result == nil) {
			print("SecItemCopyMatching found nothing");
			return;
		}

		let dic = result as! NSDictionary
		let user = dic[kSecAttrAccount] ?? ""
		let passData = dic[kSecValueData] as! Data
		let pass = String(data: passData, encoding: .utf8)!

		self.user = user as! String;
		self.pass = pass;

		self.needs_save = false;
		self.save_auth = true;
	}

	func saveToKeychain(host: String) {
		let keychainItem = [
		  kSecValueData: pass.data(using: .utf8)!,
		  kSecAttrAccount: self.user,
		  kSecAttrServer: host,
		  kSecClass: kSecClassInternetPassword
		] as CFDictionary;

		let status = SecItemAdd(keychainItem, nil);

		print("Saving operation finished with status: \(status)")
	}

	func deleteFromKeychain(host: String) {
		let query = [
		  kSecClass: kSecClassInternetPassword,
		  kSecAttrServer: host,
		  kSecReturnAttributes: true,
		  kSecReturnData: true
		] as CFDictionary

		let status = SecItemDelete(query);

		print("Deletion operation finished with status: \(status)")
	}

	func openDoor(url: String) {
		DispatchQueue.main.async {
			self.msg = "";
		}

		// check we have some credentialsc
		guard !self.user.isEmpty && !self.pass.isEmpty else {
			return
		}

		// construct URL object
		guard let url_obj = URL(string: url) else {
			print("bad URL: \(url)");
			return;
		}

		// build an HTTP request as their is no basic auth support so we need a way to add our own headers
		var request = URLRequest(url: url_obj);

		let authData = (user + ":" + pass).data(using: .utf8)!.base64EncodedString();
		request.addValue("Basic \(authData)", forHTTPHeaderField: "Authorization");

		working = true;

		let sess = URLSession.init(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil);
		let task = sess.dataTask(with: request) { data, response, error in
			DispatchQueue.main.async {
				self.working = false;
			}
			if let error = error {
				print("got error in complete")
				DispatchQueue.main.async {
					self.msg = error.localizedDescription;
				}
				return;
			}
			guard let httpResponse = response as? HTTPURLResponse else {
				DispatchQueue.main.async {
					self.msg = "non-HTTP response: \(String(describing: response))";
				}
				return;
			}
			if (httpResponse.statusCode < 200 || httpResponse.statusCode > 299) {
				var w_msg = "HTTP error: \(httpResponse.statusCode)";
				if let data = data {
					w_msg += " " + String(decoding: data, as: UTF8.self);
				}
				DispatchQueue.main.async {
					self.msg = w_msg;
				}
				return;
			}

			// things are probably ok
			if let data = data {
				let as_string = String(decoding: data, as: UTF8.self);
				DispatchQueue.main.async {
					self.msg = as_string;
				}
			}
			DispatchQueue.main.async {
				self.success = true;
			}

			if (self.save_auth) {
				if (self.needs_save) {
					self.saveToKeychain(host: self.host);
				}
			} else {
				self.deleteFromKeychain(host: self.host);
			}
		};
		task.resume();
	}

	func urlSession(_ session: URLSession, didReceive: URLAuthenticationChallenge, completionHandler: (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {

		let protectionSpace = didReceive.protectionSpace;
		guard protectionSpace.authenticationMethod ==
				NSURLAuthenticationMethodServerTrust,
			  protectionSpace.host.contains("door.local") else {
			completionHandler(.performDefaultHandling, nil)
			return
		}

		guard let serverTrust = protectionSpace.serverTrust else {
			completionHandler(.performDefaultHandling, nil)
			return
		}

		let credential = URLCredential(trust: serverTrust)
		completionHandler(.useCredential, credential)
	}
}
