//
//  ContentView.swift
//  FrontDoor
//
//  Created by Andrew Stevenson on 14.05.22.
//

import Security
import SwiftUI

struct MainView: View {
	@Environment(\.scenePhase) var scenePhase;
	@ObservedObject var model: Model;


	private var needs_save: Bool = true;

	init() {
		self.model = Model.init(fromKeychain: "door.local");
	}

	var body: some View {
		VStack(
			alignment: .leading,
			spacing: 10
		) {
			Form {
				Text("Front Door")
					.font(.title)
				Section(header: Text("Authentication")) {
					TextField("Username", text: $model.user)
						.autocapitalization(.none)
						.disableAutocorrection(true);

					SecureField("Password", text: $model.pass);
					Toggle(isOn: $model.save_auth) {
						Text("Remember credentials");
					}
				}
				Button("Let Me In!") {
					model.openDoor(url: "https://door.local/door/unlock");
				}
				if (!model.msg.isEmpty) {
					Section(header: Text("Response")) {
						Text(model.msg);
					}.onChange(of: scenePhase) { newPhase in
						if newPhase == .inactive {
							if (!model.working) {
								model.msg = "";
							}
						}
					}
				}
			}.padding()
		}
	}

//	func viewDidLoad() {
//		super.viewDidLoad()
//
//		NotificationCenter.default.addObserver(self,
//											   selector: #selector(handleAppDidBecomeActiveNotification(notification:)),
//											   name: UIApplication.didBecomeActiveNotification,
//											   object: nil)
//	}
}

struct MainView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			MainView()
		}
	}
}
